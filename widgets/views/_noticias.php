<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row row-flex-wrap">

<?php
    foreach ($modelos as $modelo) {
?>
    
<div class="site-index">

    <div class="col-sm-6 col-md-6">
        <div class="thumbnail">
            <div class="caption">
                <figure>
                    <?= Html::img("@web/imgs/$modelo->foto", ['class' => 'img-responsive']); ?>
                </figure>
                <h3> <?= $modelo->titulo ?> </h3>
                <p> <?= $modelo->texto ?> </p>
            </div>
        </div>
    </div> 

</div>
<?php
    }
?>

