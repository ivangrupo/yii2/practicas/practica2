<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

?>


<div class="row row-flex-wrap">

<?php
    foreach ($modelos as $modelo) {
?>
    
<div class="site-index">

    <div class="col-sm-4 col-md-4">
        <div class="thumbnail">
            <div class="caption">
                <h3> <?= $modelo->titulo ?> </h3>
                <p> <?= $modelo->textoLargo ?> </p>
                <figure>
                    <?= Html::img("@web/imgs/$modelo->foto", ['class' => 'img-responsive']); ?>
                </figure>
            </div>
        </div>
    </div> 

</div>
<?php
    }
?>

