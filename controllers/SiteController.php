<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Noticias;
use app\models\Articulos;



class SiteController extends Controller{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
//    public function actionTodo() {
//        
//            $dataProvider = $dataProviderNot;
//            
//            $dataProviderNot = new ActiveDataProvider([
//                'query' => Noticias::find(),
//                'pagination'=>[
//                    'pageSize'=>'2',
//                ]
//            ]);
//        
//            $dataProvider = $dataProviderArt;
//        
//            $dataProviderArt = new ActiveDataProvider([
//                'query' => Articulos::find(),
//                'pagination'=>[
//                    'pageSize'=>'2',
//                ]
//            ]);
//
//            return $this->render('todo', [
//                'dataProvider' => $dataProviderNot,
//                'dataProvider' => $dataProviderArt,
//            ]);
//            
//        }
    
}
