<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Artículos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row row-flex-wrap">

<?php
    foreach ($modelos as $modelo) {
?>
    
<div class="site-index">

    <div class="col-sm-4 col-md-4">
        <div class="thumbnail">
            <div class="caption">
                <figure>
                    <?= Html::img("@web/imgs/$modelo->foto", ['class' => 'img-responsive']); ?>
                </figure>
                <h3> <?= $modelo->titulo ?> </h3>
                <p> <?= $modelo->texto ?> </p>
                <p> <?= Html::a('<span class="btn btn-primary" role="button">Leer Más</span>',[
                            'articulos/_articulo',"id"=>$modelos->modelos]); ?></p>
            </div>
        </div>
    </div> 

</div>
<?php
    }
?>